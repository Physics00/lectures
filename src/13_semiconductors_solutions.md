```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 13 exercises

## Exercise 1: Energy, mass, velocity and cyclotron motion of electrons and holes

### Subquestion 1

Electrons near the top of the valence band have a negative effective mass, their energy decreases as $k$ increases from 0, and they have a negative group velocity for $k>0$.

### Subquestion 2

Holes near the top of the valence band have a positive effective mass, their energy increases as $k$ increases from 0, and they have a negative group velocity for $k>0$.

### Subquestion 3

The equation of motion for an electron near the bottom of the conduction band is:
$$m_{eff} \frac{d\mathbf{v}}{dt} = -e\mathbf{v} \times \mathbf{B}$$
and when replacing we get two coupled equations:
$$\dot{k_x} = \frac{e}{m_eff}B k_y$$
$$\dot{k_y} = -\frac{e}{m_eff}B k_x$$

The solution to this equation is circular motion of cyclotron frequency of $\omega_c = \frac{eB}{m_{eff}}$, where the Lorentz
force is perpendicular to $\nabla_\mathbf{k} E$.


### Subquestion 4

A hole near the bottom of the conduction band will have the same chirality as an electron.
The chirality would be just the opposite if we now consider the valence band (for both electrons and holes).



## Exercise 2: holes in Drude and tight binding model

### Subquestion 1

Write the [Hall Effect Equations](/3_drude_model/#hall-effect) for holes and conclude by analysing charge sign.

### Subquestion 2
In general,
$$\rho_{xy} = \frac{B(B^2R_e R_h(R_e + R_h)+R_h r_e^2 + R_e r_h^2}{B^2(R_e+R_h)^2+(r_e+r_h)^2}$$
where $r=n\mu$ and $R=q/n$ for both electrons and holes. This result can be obtained by using $1/\rho = 1/\rho_e + 1/\rho_h$.
When considering equal concentrations, $\rho_{xy}=0$.

### Subquestion 3

$$m_e = -\frac{\hbar^2}{2ta^2cos(ka)}$$
$$v_e = -\frac{2tasin(ka)}{\hbar}$$
$$m_h = -m_e$$
$$v_h = v_e$$
The effective masses will be of opposite sign, while the group velocities will be the same!

### Subquestion 4

$$ n_h = \int_{\varepsilon-2t}^{\varepsilon+2t} (1-f(\varepsilon)) g_h(\varepsilon)d\varepsilon$$

### Subquestion 5





## Exercise 3: a 1D semiconductor

```python
def dispersion(EG, tcb, tvb, N=100, kmax=np.pi/2):
    a = 1
    kx = np.linspace(-kmax, kmax, N)
    Ecb = EG - 2*tcb*(np.cos(kx*a)-1)
    Evb = 2*tvb*(np.cos(kx*a)-1)
    
    # Plot dispersion
    plt.figure(figsize=(6,5))
    cb, = plt.plot(kx, Ecb, label="Conduction B.")
    vb, = plt.plot(kx, Evb, label="Valence B.")

    plt.xlabel('$k_x$', fontsize=20)
    plt.ylabel('$E$', fontsize=20)
    plt.title('E(k) for tcb:'+str(tcb)+' tvb:'+str(tvb))
    plt.legend(handles=[cb, vb])
    
    plt.show()

dispersion(10, 2, 8)
```

### Subquestion 1
Apply the following to valence and conduction band, respectively:
$$v=\hbar^{-1}\partial E(k)/\partial k$$
$$m_{eff} = \hbar^2\left(d^2 E(k)/dk^2\right)^{-1}$$

Be careful with hole calculations.

$$v_e = \frac{2at_{cb}}{\hbar}sin(ka) $$
$$m_e =  \frac{\hbar^2}{2a^2t_{cb}cos(ka)}$$

$$v_h = -\frac{2at_{vb}}{\hbar}sin(ka) $$
$$m_h =  \frac{\hbar^2}{2a^2t_{vb}cos(ka)}$$

### Subquestion 2
This approximation indicates the chemical potential is "well bellow" the conduction band and "well above"
the valence band. This way, we only have a few electrons and a few holes per band, which allows us to
approximate Fermi statistics by Boltzmann's.

Assume $t_{cb}$ and $t_{vb}$ positive.

For electrons, do a taylor expansion around $k=0$ (min) of $E_{cb} = E_G - 2 t_{cb} [\cos(ka)-1],$.
For holes, do a taylor expansion around $k=0$ (max) of $E_{vb} = 2 t_{vb} [\cos(ka)-1]$.
Near $k=0$
$$ E_{cb} = E_G + t_{cb}(ka)^2$$
$$ E_{vb} = -t_{vb}(ka)^2 $$

### Subquestion 3

Now, $g_e$ and $g_h$ can be calculated.
??? hint "How?"
    The approximated bands are quadratic! We've seen these before during the lecture.


### Subquestion 4

$$n_e = \int_{E_{cb}}^{E_{cb}+4t_{cb}} f(\varepsilon)g_c(\varepsilon)d\varepsilon$$
$$n_h = \int_{-4t_{vb}}^{0} (1-f(\varepsilon))g_h(\varepsilon)d\varepsilon$$


### Subquestion 5
For an intrinsic semiconductor the number of electrons excited into the conduction band must be equal
to the number of holes left behind in the valence band, so $p = n$. Impose this condition with the results of 
Subquestion 4.

