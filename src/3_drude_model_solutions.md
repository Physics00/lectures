# Solutions for Drude model exercises

### Exercise 1: Extracting quantities from basic Hall measurements

1.

Hall voltage is measured across the sample width. Hence,

$$
V_H = -\int_{0}^{W} E_ydy
$$

where $E_y = -v_xB$.

$R_{xy}$ = $-\frac{B}{ne}$, so it does not depend on the sample geometry.


2.

If hall resistance and magnetic field are known, the charge density is calculated from $R_{xy} = -\frac{B}{ne}$.

As $V_x = -\frac{I_x}{ne}B$, a stronger field makes Hall voltages easier to measure.

3.

$$
R_{xx} = \frac{\rho_{xx}L}{W}
$$ 

where $ \rho_{xx} = \frac{m_e}{ne^2\tau}$. Therefore, scattering time ($\tau$) is known and $R_{xx}$ depend upon the sample geometry.

### Exercise 2: Motion of an electron in a magnetic and an electric field

1.

$$
m\frac{d\bf v}{dt} = -e(\bf v \times \bf B)
$$

Magnetic field affects only the velocities along x and y, i.e., $v_x(t)$ and $v_y(t)$ as they are perpendicular to it. Therefore, the equations of motion for the electron are

$$
\frac{dv_x}{dt} = -\frac{ev_yB_z}{m}
$$

$$
\frac{dv_y}{dt} = \frac{ev_xB_z}{m}
$$

2.

We can compute $v_x(t)$ and $v_y(t)$ by solving the differential equations in 1.

From 
$$
v_x'' = -\frac{e^2B_z^2}{m^2}v_x
$$
and the initial conditions, we find $v_x(t) = v_0 \cos(\omega_c t)$ with $\omega_c=eB_z/m$. From this we can derive $v_y(t)=v_0\sin(\omega_c t)$. 

We now calculate the particle position using $x(t)=x(0) + \int_0^t v_x(t')dt'$ (and similar for $y(t)$). From this we can find a relation between the $x$- and $y$-coordinates of the particle 
$$
(x(t) - x_0)^2 + (y(t) - y_0)^2 = \frac{v_0^2}{\omega_c^2}.
$$
This equation describes a circular motion around the point $x_0=x(0), y_0=y(0)+v_0/\omega$, where the characteristic frequency $\omega_c$ is called the *cyclotron* frequency. Intuition: $\frac{mv^2}{r} = evB$ (centripetal force = Lorentz force due to magnetic field).

3.

Due to the applied electric field $\bf E$ in the $x$-direction, the equations of motion acquire an extra term:
$$
m v_x' = -e(E_x + v_yB_z).
$$
Differentiating w.r.t. time leads to the same 2nd-order D.E. for $v_x$ as above. However, for $v_y$ we get
$$
v_y'' = -\omega_c^2(v_d+v_y),
$$ 
where we defined $v_d=\frac{E_x}{B_z}$. The general solutions are
$$
v_y(t) = c_1\sin(\omega_c t)+ c_2\cos(\omega_c t) -v_d \\
v_x(t) = c_3\sin(\omega_c t)+ c_4\cos(\omega_c t).
$$
Using the initial conditions $v_x(0)=v_0$ and $v_y(0)=0$ and the 1st order D.E. above, we can show
$$
v_y(t) = v_0\sin(\omega_c t)+ v_d\cos(\omega_c t) -v_d \\
v_x(t) = v_d\sin(\omega_c t)+ v_0\cos(\omega_c t).
$$

By integrating the expressions for the velocity we find:
$$
(x(t)-x_0)^2 + (y(t) - y_0 + v_d t))^2 = \frac{v_0^2}{\omega_c^2}.
$$

This represents a [cycloid](https://en.wikipedia.org/wiki/Cycloid#/media/File:Cycloid_f.gif): a circular motion around a point that moves in the $y$-direction with velocity $v_d=\frac{E_x}{B_z}$.

<!---
4.
See 3_drude_model.md

$$
m\left(\frac{d\bf v}{dt} + \frac{\bf v}{\tau}\right) = -e(\bf E + \bf v \times \bf B)
$$
-->

### Exercise 3: Temperature dependence of resistance in the Drude model

1.

Find electron density from $n_e =   \frac{ZnN_A}{W} $ 

where *Z* is valence of copper atom, *n* is density, $N_A$ is Avogadro constant and *W* is atomic weight. Use $\rho$ from the lecture notes to calculate scattering time.

2.

$\lambda = \langle v \rangle\tau$

3.

Scattering time $\tau \propto \frac{1}{\sqrt{T}}$; $\rho \propto \sqrt{T}$

4.

In general, $\rho \propto T$ as the phonons in the system scales linearly with T (remember high temperature limit of Bose-Einstein factor becomes $\frac{kT}{\hbar\omega}$ leading to $\rho \propto T$). Inability to explain this linear dependence is a failure of the Drude model.

### Exercise 4: The Hall conductivity matrix and the Hall coefficient

1.

$\rho_{xx}$ is independent of B and

$\rho_{xy} \propto B$

2, 4.

Refer to the lecture notes

3.

$$
\sigma_{xx} = \frac{\rho_{xx}}{\rho_{xx}^2 + \rho_{xy}^2}
$$

$$
\sigma_{xy} = \frac{-\rho_{yx}}{\rho_{xx}^2 + \rho_{xy}^2}
$$

This describes a [Lorentzian](https://en.wikipedia.org/wiki/Spectral_line_shape#Lorentzian).
